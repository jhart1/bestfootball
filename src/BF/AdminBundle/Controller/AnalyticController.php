<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-17
 * Time: 10:58 PM
 */

namespace BF\AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AnalyticController extends Controller
{
    public function tilesAction()
    {
        return $this->render('BFAdminBundle:Partials:top_tiles.html.twig');
    }
}