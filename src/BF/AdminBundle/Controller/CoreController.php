<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-17
 * Time: 10:29 PM
 */

namespace BF\AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoreController extends Controller
{
    public function indexAction()
    {
        return $this->render('BFAdminBundle:Core:index.html.twig');
    }
}