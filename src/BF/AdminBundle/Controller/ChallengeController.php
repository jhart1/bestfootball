<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-17
 * Time: 11:55 PM
 */


namespace BF\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use BF\AppBundle\Entity\Challenge;
use BF\AppBundle\Form\ChallengeType;

class ChallengeController extends Controller
{
    /**
     *
     * @return Returns the list off all the challenges from the newest to the oldest one.
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('BFAppBundle:Challenge');
        $challenges = $repository->findAll(
            array('date' => 'ASC')
        );

        return $this->render('BFAdminBundle:Challenge:index.html.twig', array(
            'challenges' => $challenges,
        ));
    }

    /**
     *
     * view a challenge
     */
    public function viewAction()
    {
        return $this->render('BFAdminBundle:User:view.html.twig');
    }

    /**
     *
     * add a challenge
     */
    public function addAction(request $request)
    {
        $challenge = new Challenge();
        $form = $this->createForm(ChallengeType::class, $challenge);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $challenge->setClubPublic(0);
            $em->persist($challenge);
            $em->flush();

            //Call the convert command to convert the video. Asynchronously
            $command = '/usr/bin/php '.$this->get('kernel')->getRootDir().'/../bin/console app:convert-video:challenge --ID=' . $challenge->getId()." > /dev/null 2>/dev/null &";
            shell_exec($command);

            return $this->redirectToRoute('bf_admin_challenge_index');
        }

        return $this->render('BFAdminBundle:Challenge:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * modify a challenge
     */
    public function modAction(Challenge $challenge, request $request)
    {
        $form = $this->createForm(ChallengeType::class, $challenge);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            if($challenge->getSponsored() == 0){
                $challenge->setPartner(null);
            }

            $em->persist($challenge);
            $em->flush();

            return $this->redirectToRoute('bf_admin_challenge_index');
        }

        return $this->render('BFAdminBundle:Challenge:mod.html.twig', array(
            'form' => $form->createView(),
            'challenge' => $challenge
        ));
    }

    /**
     * Delete a challenge
     *
     */
    public function delAction(Challenge $challenge)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($challenge);
        $em->flush();
        return $this->redirectToRoute('bf_admin_challenge_index');
    }
}