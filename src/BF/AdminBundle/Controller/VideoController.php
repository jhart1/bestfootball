<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-17
 * Time: 11:47 PM
 */

namespace BF\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BF\UserBundle\Entity\User;

class VideoController extends Controller
{
    /**
     *
     * @return Returns the list off all the videos from the newest to the oldest one.
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('BFUserBundle:User');
        $users = $repository->findAll(
            array('username' => 'ASC')
        );

        return $this->render('BFAdminBundle:User:index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     *
     * view a user
     */
    public function viewAction()
    {
        return $this->render('BFAdminBundle:User:view.html.twig');
    }

    /**
     *
     * modify a user
     */
    public function modAction(User $user)
    {
        return $this->render('BFAdminBundle:User:index.html.twig');
    }

    /**
     * Delete a user and all his activity
     *
     */
    public function delAction(User $user)
    {
        return $this->render('BFAdminBundle:User:index.html.twig');
    }
}