<?php

namespace BF\AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BFAdminBundle:Default:index.html.twig');
    }
}