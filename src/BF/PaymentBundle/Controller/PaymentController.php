<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2017-01-07
 * Time: 12:55 PM
 */

namespace BF\PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;

class PaymentController extends Controller
{
    public function preparePaymentAction(Request $request)
    {

        //Check if the user already entered the club and account information
        $session = $request->getSession();
        $new_user = $session->get('new_user');
        $new_club = $session->get('new_club');
        if( !isset($new_user) && !isset($new_club) || (empty($new_user) && !empty($new_club))){
            //@TODO Create a flashbag mesage with the information shy the payment is not autorised.
            $this->addFlash(
                'error',
                'Renseigner votre compte et votre club'
            );

            return $this->redirectToRoute('bf_core_bestclub_subscription');
        }

        $secret = $this->getParameter('stripe.secret');
        $publishable = $this->getParameter('stripe.publishable');


        //Form for the user and the club
        $form = $this->createFormBuilder()
            ->add('number' , TextType::class)
            ->add('exp_month', IntegerType::class)
            ->add('exp_year', IntegerType::class)
            ->add('cvc', IntegerType::class)
            ->add('customer_name', TextType::class)
            ->add('customer_email', TextType::class)
            ->add('customer_address',TextType::class)
            ->add('customer_zip',TextType::class)
            ->add('customer_city',TextType::class)
            ->add('customer_country',TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if($form->isValid()){
            //Store the data of the 2 forms into
            $data = $form->getData();

            //create the different arrays

            $customer_card = array(
                'object' => "card",
                'exp_month' => $data['exp_month'],
                'exp_year' => $data['exp_year'],
                'number' => $data['number'],
                'currency' => 'EUR',
                'cvc' => '123',
            );


            //Prepare the payment
            \Stripe\Stripe::setApiKey($secret);

            //Making the request to the Stripe API
            try {
                $response = \Stripe\Customer::create(array(
                    "description" => "Customer for " . $data['customer_name'],
                    "email" => $data['customer_email'],
                    "plan" => "001",
                    "source" => $customer_card // obtained with Stripe.js
                ));


            } catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];

                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");


                //@TODO Create a flashbag mesage with the information shy the payment is not autorised.
                $this->addFlash(
                    'error',
                    $err['message']
                );

                return $this->redirectToRoute('bf_payment_make');

            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
            }

            $em = $this->getDoctrine()->getManager();
            $userManager = $this->get('fos_user.user_manager');
            $club_user = $userManager->createUser();
            $club_user
                ->setEnabled(true)
                ->setUsername($new_user['username'])
                ->setEmail($new_user['email'])
                ->setPlainPassword($new_user['password'])
                ->addRole('ROLE_CLUB')
                ->setClub($new_club)
            ;

            //If the payment is accepted, create the new user.
            $session = $request->getSession();
            $em->persist($club_user);
            $em->persist($new_club);
            $em->flush();

            //@TODO Send a mail with confirmation of the account and bill.

            //Redirect the uer to the done page.
            return $this->redirectToRoute('bf_payment_done');
        }


        return $this->render('BFPaymentBundle:Payment:make.html.twig',array(
            'form' => $form->createView()
        ));

    }

    public function doneAction(Request $request)
    {

    }
}