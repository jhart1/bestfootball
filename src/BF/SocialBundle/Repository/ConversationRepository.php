<?php

namespace BF\SocialBundle\Repository;

use BF\UserBundle\Entity\User;

/**
 * ConversationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ConversationRepository extends \Doctrine\ORM\EntityRepository
{

    public function getConversations(User $user, $offset, $limit)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->innerJoin('c.users', 'u')
            ->innerJoin('c.messages', 'm')
            ->where('u = :user')
            ->setParameter('user', $user)
            ->orderBy('m.date', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }
}
