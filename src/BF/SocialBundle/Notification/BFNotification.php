<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-31
 * Time: 10:28 PM
 */

namespace BF\SocialBundle\Notification;

use BF\SocialBundle\Entity\Notification;
use BF\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class BFNotification
{
    /**
     * Create Notifications and save them
     *
     */

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function create($user, $sender, $message, $link)
    {
        //we create a notification.
        $notification = new Notification();
        $notification
            ->setDate(new \Datetime())
            ->setMessage($message)
            ->setReceiver($user)
            ->setLink($link)
            ->setViewed('0')
        ;

        if($sender !== null){
            $notification->setSender();
        }

        $this->em->persist($notification);
        $this->em->flush();

        return true;
    }
}