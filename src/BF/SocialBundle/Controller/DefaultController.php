<?php

namespace BF\SocialBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BFSocialBundle:Default:index.html.twig');
    }
}
