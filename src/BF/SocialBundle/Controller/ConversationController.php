<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-12-11
 * Time: 10:52 PM
 */

namespace BF\SocialBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BF\SocialBundle\Entity\Conversation;
use BF\SocialBundle\Entity\Message;
use BF\UserBundle\Entity\User;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ConversationController extends Controller
{
    /**
     *
     * This method is called by AJAX when a user sends a message to a whole NEW conversation
     *
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createConversationAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->getUser();
        $receivers = json_decode(stripslashes( $request->request->get('receivers')));

        $message = new Message();
        $message
            ->setMessage('Ceci est un message de test !!')
            ->setDate(new \DateTime())
            ->setSender($user)
        ;

        //Set the message and users for the new conversation.
        $conversation = new Conversation();
        $conversation
            ->addUser($user)
            ->addMessage($message)
            ->setLastMessage(new \DateTime())
        ;

        //Adding the receivers to the conversation.
        foreach ($receivers as $receiver_id){
            $receiver = $em->getRepository('BFUserBundle:User')->find($receiver_id);
            if( is_a($receiver, User::class)){
                $conversation->addUser($receiver);
            }
        }

        //Set the conversation for the message
        $message->setConversation($conversation);

        $em->persist($conversation);
        $em->persist($message);
        $em->flush();

        return new JsonResponse();
    }

    /**
     *
     * This method is called by AJAX when a user asks to look at a conversation.
     *
     * @param Request $request
     */
    public function getConversationAction(Request $request){
        $em = $this->getDoctrine()->getEntityManager();

    }

    /**
     *
     * This method is called by AJAX when a user looks at the list of past conversations.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getConversationsAction(Request $request){
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->getUser();


        //Hard coded, change to receive these parameters.
        $offset = $request->request->get('offset');
        $limit = $request->request->get('limit');

        $latestConversations = $em->getRepository('BFSocialBundle:Conversation')->getConversations($user, $offset, $limit);

        $conversations = array();

        //Get the last message for every conversation
        foreach ($latestConversations as $latestConversation) {

            $lastMessage = $em->getRepository('BFSocialBundle:Message')->getLastMessage($latestConversation);
            if($lastMessage->getViewed() == 0 && $lastMessage->getSender()->getId() != $user->getId()){
                $read = 0;
            }
            else{
                $read = 1;
            }

            $participants = $latestConversation->getUsers();

            //Check if it is a conversation between 2 users.
            if(count($participants) == 2){
                foreach ($participants as $participant){
                    if($user->getId() != $participant->getId()){
                        $otherUser = $participant;
                    }
                }
            }



            $conversation = array(
                'otherUsername' => $otherUser->getUsername(),
                'otherUserAvatar' => $otherUser->getProfilePicture()->getSrc(),
                'lastMessage' => $lastMessage->getMessage(),
                'lastMessageDate' => $lastMessage->getDate()->format('d-m-Y'),
                'read' => $read
            );

            array_push($conversations, $conversation);
        }

        $response = new JsonResponse();
        $response->setData($conversations);
        return $response;

    }


    public function getFrontConversationsAction(){

        $user = $this->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $conversations = $em->getRepository('BFSocialBundle:Conversation')->getConversations($user, 0, 150);


        //get the details of one challenge
        return $this->render('BFSocialBundle:Conversation:all.html.twig', array(
            'conversations' => $conversations,
        ));
    }

    /**
     *
     * @ParamConverter("conversation", options={"mapping": {"id": "id"}})
     *
     */
    public function getFrontConversationAction(Conversation $conversation){

        //check if the user can request the conversation.
        $participants = $conversation->getUsers();
        $user = $this->getUser();

        if(is_array($participants) && !in_array($user, $participants)){
            throw new AccessDeniedException();
        }

        //get the details of one challenge
        return $this->render('BFSocialBundle:Conversation:single-conversation.html.twig', array(
            'conversation' => $conversation,

        ));
    }
}
