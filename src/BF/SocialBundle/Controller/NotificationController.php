<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-12-08
 * Time: 10:32 PM
 */

namespace BF\SocialBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BF\SocialBundle\Entity\Notification;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NotificationController extends Controller
{

    public function getAjaxNotificationsAction(Request $request){

        $em = $this->getDoctrine()->getEntityManager();

        $limit = $request->request->get('limit');
        $offset = $request->request->get('offset');
        $user = $this->getUser();

        $notifications = $em->getRepository('BFSocialBundle:Notification')->getNotifications($user, $offset, $limit);

        $notif_response = [];

        foreach ($notifications as $notification){
            if(is_a($notification, Notification::class)){
                if($notification->getSender() != null){
                    $senderLink = $this->generateUrl('bf_user_profil',array('username' => $notification->getSender()->getUsername()));
                    $senderAvatar = $notification->getSender()->getProfilePicture()->getSrc();
                }
                else{
                    $senderLink = null;
                    $senderAvatar = 'http://bestfootball.fr/img/logo.png';
                }
                $notif_response[] =  array(
                    'sender_link' => $senderLink,
                    'sender_avatar' => $senderAvatar,
                    'message' => $notification->getMessage(),
                    'link' => $notification->getLink(),
                    'viewed' => $notification->getViewed(),
                    'time_passed' => $this->humanTiming($notification->getDate()->getTimestamp()),
                    'id' => $notification->getId(),
                );
            }
        }


        $response = new JsonResponse();
        $response->setData($notif_response);
        return $response;

    }

    public function getAjaxNotViewedNotificationsNumberAction(Request $request){

        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->getUser();
        $notifications = $em->getRepository('BFSocialBundle:Notification')->getNotViewedNotifications($user);
        $numberNotifications = count($notifications);


        $response = new JsonResponse();
        $response->setData($numberNotifications);
        return $response;

    }

    public function markNotificationReadAction(Request $request){

        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->getUser();
        $notification = $em->getRepository('BFSocialBundle:Notification')->find($request->request->get('id'));
        $notification->setViewed(true);
        $em->flush();

        $response = new JsonResponse();
        return $response;

    }

    private function humanTiming ($time)
    {

        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

    }
}