<?php

namespace BF\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BFApiBundle:Default:index.html.twig');
    }
}
