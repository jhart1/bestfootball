<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-31
 * Time: 8:39 PM
 */

namespace BF\CoreBundle\Controller;

use BF\AppBundle\BFAppBundle;
use BF\AppBundle\Entity\PlayerSeasonStats;
use BF\AppBundle\Entity\Season;
use BF\AppBundle\Entity\Team;
use BF\AppBundle\Entity\TeamInvitation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class TeamController extends Controller
{

    /**
     * The page where the coach or club can see his team(s).
     *
     */
    public function indexAction($season)
    {
        $em = $this->getDoctrine()->getManager();

        //check the role of the user trying to access the page
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COACH')){
            // Get the season.
            if($season == ''){
                //Get the current season.
                $season = $em->getRepository('BFAppBundle:Season')->currentSeason();
            }
            else{
                $season = $em->getRepository('BFAppBundle:Season')->findOneByName($season);
            }

            //Coach is looking at his page. Overview of all his teams of this season
            $coach = $this->getUser();
            $team = $coach->getTeam();

            //get the players of the team in the current season.
            $players = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:PlayerSeasonStats')->currentSeasonPlayers($season, $team);

            //Get the current and future seasons
            $seasons = $em->getRepository('BFAppBundle:Season')->currentAndFutureSeasons();

            return $this->render('BFCoreBundle:Team:index-coach.html.twig',array(
                'team' => $team,
                'players' => $players,
                'seasons' => $seasons
            ));
        }
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_USER')){

            //The player is looking at this page. Show him his team.

            return $this->render('BFCoreBundle:Team:index-player.html.twig');
        }


    }

    /**
     * @param TeamInvitation $teamInvitation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewInvitationAction(Request $request, TeamInvitation $teamInvitation)
    {
        //Send the hash of the invitation to the page.
        $hash = $teamInvitation->getHash();
        $club = $teamInvitation->getTeam()->getClub();

        if($teamInvitation->getUser()->getId() != $this->getUser()->getId()){
            throw $this->createAccessDeniedException();
        }

        if($teamInvitation->getActive() === false){

            $this->addFlash(
                'error',
                'Vous avez deja repondu a cette invitation'
            );

            return $this->redirectToRoute('bf_core_homepage');
        }

        return $this->render('BFCoreBundle:Team:player-invitation.html.twig',array(
            'invitation' => $teamInvitation,
            'club' => $club,
            'team' => $teamInvitation->getTeam()
        ));
    }



    public function teamViewAction($teamslug, $season){

        $em = $this->getDoctrine()->getManager();
        $team = $em->getRepository('BFAppBundle:Team')->findOneBy(array('slug' => $teamslug));

        if($season == ''){
            $season = $em->getRepository('BFAppBundle:Season')->currentSeason();
        }
        else{
            $season = $em->getRepository('BFAppBundle:Season')->findOneByName($season);
        }

        //get the players of the team in the current season.
        $players = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:PlayerSeasonStats')->currentSeasonPlayers($season, $team);

        return $this->render('BFCoreBundle:Team:view.html.twig',array(
            'team' => $team,
            'players' => $players
        ));













        if ($this->get('security.authorization_checker')->isGranted('ROLE_COACH')){

            //Coach is looking at his page. Show all the challenges of his team. Also let him add, mod or delete challenges,
            $coach = $this->getUser();
            $team = $coach->getTeam();

            $teamChallenges = $em->getRepository('BFAppBundle:Challenge')->findBy(array('team' => $team));


            return $this->render('BFCoreBundle:Team:challenges-coach.html.twig',array(
                'teamChallenges' => $teamChallenges,
            ));
        }
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_USER')){

            //The player is looking at this page. Show him his team.
            $season = $em->getRepository('BFAppBundle:Season')->currentSeason();
            $user = $this->getUser();

            $playerSeasonStats = $em->getRepository('BFAppBundle:PlayerSeasonStats')->findOneBy(array('user' => $user, 'season' => $season));
            $team = $playerSeasonStats->getTeam();

            //Get the Team challenges
            $teamChallenges = $em->getRepository('BFAppBundle:Challenge')->findBy(array('team' => $team));

            //Get the Public Club challenges


            $clubTeams = $team->getClub()->getTeams();

            $clubChallenges = array();
            if($clubTeams != null){
                foreach ($clubTeams as $clubTeam){
                    $teamChallenges = $em->getRepository('BFAppBundle:Challenge')->findBy(array('team' => $clubTeam, 'club_public' => true));

                    array_push($clubChallenges, $teamChallenges);
                }
            }

            return $this->render('BFCoreBundle:Team:index-player.html.twig',array(
                'teamChallenges' => $teamChallenges,
                'clubChallenges' => $clubChallenges
            ));
        }
    }

    public function teamChallengesAction($teamslug, $season){
        $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $team = $em->getRepository('BFAppBundle:Team')->findOneBy(array('slug' => $teamslug));

        if($season == ''){
            $season = $em->getRepository('BFAppBundle:Season')->currentSeason();
        }
        else{
            $season = $em->getRepository('BFAppBundle:Season')->findOneByName($season);
        }

        $teamChallenges = $em->getRepository('BFAppBundle:Challenge')->findBy(array('team' => $team));

        return $this->render('BFCoreBundle:Team:challenges.html.twig',array(
            'teamChallenges' => $teamChallenges,
            'team' => $team
        ));
    }

    public function teamMatchesAction($teamslug, $season){
        $em = $this->getDoctrine()->getManager();
        $club = $this->getUser()->getClub();
        $team = $em->getRepository('BFAppBundle:Team')->findOneBy(array('slug' => $teamslug));

        $teamMatches = array();
        if($team != null){

        }

        return $this->render('BFCoreBundle:Team:matches.html.twig',array(
            'teamMatches' => $teamMatches,
            'team' => $team
        ));
    }

    public function teamTrainingsAction($teamslug, $season){
        $em = $this->getDoctrine()->getManager();
        $club = $this->getUser()->getClub();
        $team = $em->getRepository('BFAppBundle:Team')->findOneBy(array('slug' => $teamslug));

        $teamMatches = array();
        if($team != null){

        }

        return $this->render('BFCoreBundle:Team:trainings.html.twig',array(
            'teamMatches' => $teamMatches,
            'team' => $team
        ));
    }
}