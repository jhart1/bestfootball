<?php

namespace BF\CoreBundle\Controller;

use BF\AppBundle\Entity\ChallengeParticipation;
use BF\AppBundle\Entity\Club;
use BF\AppBundle\Form\ChallengeParticipationType;
use BF\AppBundle\Form\ClubType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;


class CoreController extends Controller
{
    public function indexAction(Request $request)
    {

        //if the user is connected we display another page.
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY', 'IS_AUTHENTICATED_REMEMBERED')) {
            //the user is connected. Show him his personal homepage.
            //Check the role of the user.

            $user = $this->getUser();
            $session = $request->getSession();

            //Display a flash message if the user just connected.
            $referer = $request->headers->get('referer');
            if(strpos($referer, 'login') && $session->get('fresh-login') !== false ){
                $this->addFlash( 'notice', 'Re-bonjour ' . $user->getUsername() . ' !' );
                $session->set('fresh-login', '0');
            }

            //CLUB ROLE
            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLUB')){
                return $this->redirectToRoute('bf_core_club_index');
            }
            //COACH ROLE
            elseif($this->get('security.authorization_checker')->isGranted('ROLE_COACH')){


                return $this->render('BFCoreBundle:Logged:coach.html.twig',array(

                ));
            }
            //USER ROLE
            else{
                //Get the last 4 challenges.
                $challenges = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:Challenge')->findBy(array() ,array('date' => 'DESC'), 4, 0);

                //Get the last 8 challenge participations
                $challengeParticipations = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:ChallengeParticipation')->findBy(array('public' => 1) ,array('date' => 'DESC'), 8, 0);

                return $this->render('BFCoreBundle:Logged:user.html.twig',array(
                    'challenges' => $challenges,
                    'challengeParticipations' => $challengeParticipations,
                ));
            }
        }
        else{
            //user is not connected. display the homepage.
            return $this->render('BFCoreBundle:Core:index.html.twig');
        }



    }
    public function aboutAction()
    {
        return $this->render('BFCoreBundle:Core:about.html.twig');
    }
    public function loginAction()
    {
        return $this->render('BFCoreBundle:Core:login.html.twig');
    }

    public function contactAction()
    {
        return $this->render('BFCoreBundle:Core:contact.html.twig');
    }
    public function bestclubAction()
    {
        return $this->render('BFCoreBundle:Bestclub:index.html.twig');
    }

}