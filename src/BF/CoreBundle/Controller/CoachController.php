<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2017-02-13
 * Time: 8:39 PM
 */

namespace BF\CoreBundle\Controller;

use BF\AppBundle\Entity\ChallengeParticipation;
use BF\AppBundle\Entity\Season;
use BF\AppBundle\Entity\Team;
use BF\AppBundle\Form\ChallengeParticipationType;
use BF\AppBundle\Form\ClubType;
use BF\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use BF\AppBundle\Entity\Challenge;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class CoachController extends Controller
{
    public function indexAction()
    {

    }
}