<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2017-01-14
 * Time: 12:19 PM
 */

namespace BF\CoreBundle\Controller;


use BF\AppBundle\Entity\Club;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Form\Factory\FactoryInterface;


class SubscribeController extends Controller
{
    public function subscribeAction()
    {
        return $this->render('BFCoreBundle:Subscribe:subscribe.html.twig');
    }

    public function clubSubscriptionAction(Request $request)
    {
        //Form for the user and the club
        $form = $this->createFormBuilder()
            ->add('name' , TextType::class)
            ->add('address', TextType::class)
            ->add('address2', TextType::class)
            ->add('city', TextType::class)
            ->add('zip', TextType::class)
            ->add('region', TextType::class)
            ->add('country', TextType::class)
            ->add('league', TextType::class)
            ->add('logo', EntityType::class, array(
                'class' => 'BF\AppBundle\Entity\Fichier',
            ))
            ->add('username', TextType::class)
            ->add('mail', TextType::class)
            ->add('password',TextType::class)
            ->add('password2',TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if($form->isValid()){
            //Store the data of the 2 forms into
            $data = $form->getData();
            $session = $request->getSession();

            $club = new Club();
            $club
                ->setName($data['name'])
                ->setAddress($data['address'])
                ->setAddress2($data['address2'])
                ->setCity($data['city'])
                ->setZip($data['zip'])
                ->setRegion($data['region'])
                ->setCountry($data['country'])
                ->setLeague($data['league'])
                ->setLogo($data['logo'])
            ;

            $new_user = array(
                'username' => $data['username'],
                'email' => $data['mail'],
                'password' => $data['password']
            );

            $session->set('new_user', $new_user);
            $session->set('new_club', $club);

            //Redirect the uer to the paiement page.
            return $this->redirectToRoute('bf_payment_make');
        }

        return $this->render('BFCoreBundle:Bestclub:subscription.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function playerSubscriptionAction()
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        $form = $formFactory->createForm();

        return $this->render('BFCoreBundle:Subscribe:player.html.twig',array(
            'form' => $form->createView()
        ));
    }
}