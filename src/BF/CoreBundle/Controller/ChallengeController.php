<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-23
 * Time: 8:36 PM
 */

namespace BF\CoreBundle\Controller;

use BF\AppBundle\Entity\ChallengeParticipation;
use BF\AppBundle\Form\ChallengeParticipationType;
use BF\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use BF\AppBundle\Entity\Challenge;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class ChallengeController extends Controller
{
    /**
     *
     */
    public function listChallengesAction()
    {
        //get all the challenges and display them
        $challenges  = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:Challenge')->findBy(array('active' => 1, 'team' => null), array('date' => 'DESC'));

        return $this->render('BFCoreBundle:Challenge:list.html.twig', array(
            'challenges' => $challenges,
        ));
    }

    /**
     *
     */
    public function listSponsoredChallengesAction()
    {
        //get the sponsored challenges and display the list.
        $challenges  = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:Challenge')->findBy(array('active' => 1, 'sponsored' => 1,'team' => null), array('date' => 'DESC'));

        return $this->render('BFCoreBundle:Challenge:list-sponsored.html.twig', array(
            'challenges' => $challenges
        ));
    }

    /**
     *
     */
    public function rankingAction($country, $state)
    {
        if($country == 'world'){
            //get the ranking for the entire world
            $ranking = 'world';
        }
        elseif(!empty($country) && (empty($state) || $state == '')){
            //Get the ranking for the country.
            $ranking = 'country';
        }
        else{
            //Get the ranking for a specific region

            $ranking = 'region';
        }

        //get the details of one challenge
        return $this->render('BFCoreBundle:Challenge:ranking.html.twig', array(
            'ranking' => $ranking
        ));
    }

     /**
     * @ParamConverter("challenge", options={"mapping": {"slug": "slug"}})
     */
    public function viewAction(Request $request, Challenge $challenge)
    {
        //A logged in user is watching this page
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY', 'IS_AUTHENTICATED_REMEMBERED')) {

            $participation = new ChallengeParticipation();
            $user = $this->getUser();

            //Couple the challenge and user to the particpaction.
            $participation
                ->setChallenge($challenge)
                ->setUser($user)
                ->setPublic(false)
            ;

            if($challenge->getTeam() == null){
                $participation->setPublic(true);
            }

            $form = $this->createForm(ChallengeParticipationType::class, $participation);
            $form->handleRequest($request);
            $formView = $form->createView();

            if($form->isValid()){
                $em = $this->getDoctrine()->getEntityManager();

                //Calculate the points, attribute them to the user, send out the notifications.
                $participationInfo = $this->get('bf.partcipationpoints');
                $participationScore = $participationInfo->getParticipationPoints($participation);
                $participation->setScore($participationScore);
                $userScore = $participationInfo->getUserPoints($participation, $user);
                $user->setPoints($userScore);


                //SET default thumbnail to notice the user that his video is converting.
                $participation->setThumbnail('/assets/images/convert-video.png');

                $em->persist($participation);
                $em->persist($user);
                $em->flush();


                //Call the convert command to convert the video. Asynchronously
                $command = '/usr/bin/php '.$this->get('kernel')->getRootDir().'/../bin/console app:convert-video:challenge-participation --ID=' . $participation->getId()." > /dev/null 2>/dev/null &";
                shell_exec($command);

                //@TODO: Add a flash message or notification for the user.



                //@TODO: Send a notification to all the users that did less then this participation. Make a command that sends this assynchrone



                return $this->redirectToRoute('bf_core_challenge', array(
                    'slug' => $challenge->getSlug(),
                ));
            }
        }
        else{
            $formView = null;
        }



        //Getting the last participations
        $lastParticipations = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:ChallengeParticipation')->findBy(array('challenge' => $challenge) ,array('date' => 'DESC'), 4, 0);

        //Getting the best participations
        $bestParticipations = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:ChallengeParticipation')->findBy(array('challenge' => $challenge) ,array('repetitions' => 'DESC'), 4, 0);

        //get the details of one challenge
        return $this->render('BFCoreBundle:Challenge:view.html.twig', array(
            'challenge' => $challenge,
            'form' => $formView,
            'lastParticipations' => $lastParticipations,
            'bestParticipations' => $bestParticipations
        ));

    }
}