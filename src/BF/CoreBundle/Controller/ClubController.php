<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-12-21
 * Time: 9:59 PM
 */

namespace BF\CoreBundle\Controller;

use BF\AppBundle\Entity\ChallengeParticipation;
use BF\AppBundle\Entity\Season;
use BF\AppBundle\Entity\Team;
use BF\AppBundle\Form\ChallengeParticipationType;
use BF\AppBundle\Form\ClubType;
use BF\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use BF\AppBundle\Entity\Challenge;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

class ClubController extends Controller
{
    public function indexAction()
    {
        //Get all the teams of the club.
        $club = $this->getUser()->getClub();
        $teams = $club->getActiveTeams();

        return $this->render('BFCoreBundle:Club:index.html.twig',array(
            'teams' => $teams
        ));
    }

    public function teamsAction()
    {
        //Club is looking at his page. Overview of all the teams of this season
        $user = $this->getUser();
        $club = $user->getClub();

        ///get the active teams of the club. don't add the teams that have been deactivated.
        $teams = $club->getActiveTeams();

        return $this->render('BFCoreBundle:Club:teams.html.twig',array(
            'teams' => $teams
        ));
    }

    /**
     * @param Team $team
     * @param Season $season
     *
     * This action is used for a club to view a team. He can also change the seasons for every team.
     * If no season is given, display the team for the current Season.
     *
     * @ParamConverter("team", options={"mapping": {"id": "id"}})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Team $team, $season)
    {
        $em = $this->getDoctrine()->getManager();

        if($season == ''){
            //Get the current season.
            $season = $em->getRepository('BFAppBundle:Season')->currentSeason();
        }
        else{
            $season = $em->getRepository('BFAppBundle:Season')->findOneByName($season);
        }

        //get the players of the team in the current season.
        $players = $this->getDoctrine()->getManager()->getRepository('BFAppBundle:PlayerSeasonStats')->currentSeasonPlayers($season, $team);

        return $this->render('BFCoreBundle:Team:club-view.html.twig',array(
            'team' => $team,
            'players' => $players
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function settingsAction(Request $request)
    {
        //check the role of the user trying to access the page
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_CLUB')) {
            throw new AccessDeniedException();
        }

        $club = $this->getUser()->getClub();
        $form = $this->createForm(ClubType::class, $club);
        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getEntityManager();

            $em->persist($club);
            $em->flush();

            $this->addFlash(
                'notice',
                'Vos parameters ont bien ete modifie'
            );

            return $this->redirectToRoute('bf_core_club_teams');
        }


        return $this->render('BFCoreBundle:Club:settings.html.twig',array(
            'form' => $form->createView(),
            'club' => $club
        ));
    }
}