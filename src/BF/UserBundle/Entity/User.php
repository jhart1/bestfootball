<?php

namespace BF\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;


//use the profile picture
use BF\UserBundle\Entity\ProfilePicture;

/**
 * User
 *
 * @ORM\Table(name="bf_users")
 * @ORM\Entity(repositoryClass="BF\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\OneToOne(targetEntity="BF\UserBundle\Entity\ProfilePicture", cascade={"persist"})
     */
    private $profilePicture;

    //Bidirectional relations
    /**
     * @ORM\OneToMany(targetEntity="BF\AppBundle\Entity\PlayerSeasonStats", mappedBy="user")
     */
    private $playerSeasonStatistics;

    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Club")
     * @ORM\JoinColumn(nullable=true)
     */
    private $club;

    /**
     * @ORM\OneToOne(targetEntity="BF\AppBundle\Entity\Team")
     * @ORM\JoinColumn(nullable=true)
     */
    private $team;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var date
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="cellphone", type="string", nullable=true)
     */
    private $cellphone;

    /**
     * @var string
     *
     * @ORM\Column(name="plainpwd", type="string", nullable=true)
     */
    private $plainpwd;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    public function __construct()
    {
        parent::__construct();

        //add the USER Role to the new user.
        $this->addRole("ROLE_USER");

        //Default profile picture.
        $profilePicture = new ProfilePicture();
        $src = 'https://api.adorable.io/avatars/285/' . rand(5, 1000000);
        $profilePicture->setSrc( $src );
        $this->setProfilePicture( $profilePicture );
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profilePicture
     *
     * @param \BF\UserBundle\Entity\ProfilePicture $profilePicture
     *
     * @return User
     */
    public function setProfilePicture(\BF\UserBundle\Entity\ProfilePicture $profilePicture = null)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture
     *
     * @return \BF\UserBundle\Entity\ProfilePicture
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set cellphone
     *
     * @param string $cellphone
     *
     * @return User
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    /**
     * Get cellphone
     *
     * @return string
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * Set club
     *
     * @param \BF\AppBundle\Entity\Club $club
     *
     * @return User
     */
    public function setClub(\BF\AppBundle\Entity\Club $club = null)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get club
     *
     * @return \BF\AppBundle\Entity\Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Set enabled
     *
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Set plainpwd
     *
     * @param string $plainpwd
     *
     * @return User
     */
    public function setPlainpwd($plainpwd)
    {
        $this->plainpwd = $plainpwd;

        return $this;
    }

    /**
     * Get plainpwd
     *
     * @return string
     */
    public function getPlainpwd()
    {
        return $this->plainpwd;
    }

    /**
     * Add playerSeasonStatistic
     *
     * @param \BF\AppBundle\Entity\PlayerSeasonStats $playerSeasonStatistic
     *
     * @return User
     */
    public function addPlayerSeasonStatistic(\BF\AppBundle\Entity\PlayerSeasonStats $playerSeasonStatistic)
    {
        $this->playerSeasonStatistics[] = $playerSeasonStatistic;

        return $this;
    }

    /**
     * Remove playerSeasonStatistic
     *
     * @param \BF\AppBundle\Entity\PlayerSeasonStats $playerSeasonStatistic
     */
    public function removePlayerSeasonStatistic(\BF\AppBundle\Entity\PlayerSeasonStats $playerSeasonStatistic)
    {
        $this->playerSeasonStatistics->removeElement($playerSeasonStatistic);
    }

    /**
     * Get playerSeasonStatistics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerSeasonStatistics()
    {
        return $this->playerSeasonStatistics;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return User
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set team
     *
     * @param \BF\AppBundle\Entity\Team $team
     *
     * @return User
     */
    public function setTeam(\BF\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \BF\AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }
}
