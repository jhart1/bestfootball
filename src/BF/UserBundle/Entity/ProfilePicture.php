<?php

namespace BF\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProfilePicture
 *
 * @ORM\Table(name="profile_picture")
 * @ORM\Entity(repositoryClass="BF\UserBundle\Repository\ProfilePictureRepository")
 */
class ProfilePicture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255)
     */
    private $src;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set src
     *
     * @param string $src
     *
     * @return ProfilePicture
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }
}

