<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-29
 * Time: 9:33 PM
 */

namespace BF\UserBundle\Controller;

use BF\UserBundle\Form\UserSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use BF\UserBundle\Entity\User;


class UserController extends Controller
{
    /**
     * @ParamConverter("user", options={"mapping": {"username": "username"}})
     */
    public function viewAction(User $user)
    {

        //Show the profil of a user.
        return $this->render('BFUserBundle:User:profil.html.twig',array(
            'user' => $user
        ));
    }

    public function settingsAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(UserSettingsType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('bf_user_settings');
        }

        return $this->render('BFUserBundle:User:settings.html.twig',array(
            'form' => $form->createView()
        ));
    }


    public function verifyUsernameAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('BFUserBundle:User')->findOneByUsername($request->request->get('username'));

        if($user){
            $response = false;
        }
        else{
            $response = true;
        }

        return JsonResponse::class;
    }

    public function getUserByMailAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BFUserBundle:User')->getUserLikeEmail($request->request->get('query'));

        $output = array();
        foreach($users as $user){
            $outputUser = array(
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail()
            );

            array_push($output, $outputUser);
        }

        $response = new JsonResponse();
        $response->setData(array(
            'users' => $output
        ));

        return $response;
    }

}
