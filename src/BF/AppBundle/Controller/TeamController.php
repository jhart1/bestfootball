<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-10-31
 * Time: 8:47 PM
 */

namespace BF\AppBundle\Controller;

use BF\AppBundle\Entity\PlayerSeasonStats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

//entities
use BF\AppBundle\Entity\TeamInvitation;
use BF\AppBundle\Entity\Team;
use BF\UserBundle\Entity\User;

class TeamController extends Controller
{
    /**
     *
     * Create a team
     */
    public function createTeamAction(Request $request)
    {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_CLUB')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('BFAppBundle:Club')->find($request->request->get('club_id'));

        $coach_username = str_replace(' ', '_', strtolower(substr($club->getName(),0,5) . '_' . $request->request->get('team_name')));

        //Create a random password of 6 characters.
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $coach_password = '';
        for ($i = 0; $i < 6; $i++) {
            $coach_password .= $characters[rand(0, $charactersLength - 1)];
        }

        //Create a new Coach user.

        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $coach = $userManager->createUser();
        $coach
            ->setEnabled(true)
            ->setUsername($coach_username)
            ->setEmail($coach_username . '@bestfootball.fr')
            ->setPlainPassword($coach_password)
            ->addRole('ROLE_COACH')
            ->setPlainpwd($coach_password)
        ;

        $team = new Team();
        $team
            ->setClub($club)
            ->setName($request->request->get('team_name'))
            ->setCoach($coach)
            ->setActive(true)
        ;

        $coach->setTeam($team);

        $em->persist($team);
        $em->persist($coach);
        $em->flush();


        //go back to the team page.
        return $this->redirectToRoute('bf_core_club_teams');
    }

    /**
     *
     * Create a team
     * @ParamConverter("team", options={"mapping": {"id": "id"}})
     */
    public function deleteTeamAction(Request $request, Team $team)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_CLUB')) {
            throw $this->createAccessDeniedException();
        }

        //Check if the team is part of the club.
        $user = $this->getUser();
        $club = $user->getClub();

        $teams = $club->getTeams();

        if( $teams->contains($team) ){

            //We dont delete the team to keep the player stats, we disable the team and the coach account.
            $em = $this->getDoctrine()->getManager();
            $team->setActive(false);
            $coach = $team->getCoach();
            $coach->setEnabled(false);

            $em->persist($coach);
            $em->persist($team);
            $em->flush();

            // @TODO: create a flash bag message to say the team has been deleted.

            return $this->redirectToRoute('bf_core_team');
        }
        else{
            throw $this->createAccessDeniedException();
        }
    }

    /**
     *
     * Invite a player to the team.
     */
    public function invitePlayerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $coach = $this->getUser();

        $playerEmail = $request->request->get('email_player');
        $playerTeam = $em->getRepository('BFAppBundle:Team')->find($request->request->get('team_id'));
        $playerSeason = $em->getRepository('BFAppBundle:Season')->find($request->request->get('season'));
        $playerPosition = $request->request->get('position');

        //Check if the user is already registered.
        $player = $em->getRepository('BFUserBundle:User')->findOneBy(array('emailCanonical' => $playerEmail));


        //Create an invitation.
        $invitation = new TeamInvitation();
        $invitation
            ->setTeam($playerTeam)
            ->setSeason($playerSeason)
            ->setPosition($playerPosition)
            ->setEmail($playerEmail)
            ->setActive(true)
        ;

        //The user already exists
        if($player != null){
            if($player->hasRole('ROLE_COACH') || $player->hasRole('ROLE_CLUB')){
                //@TODO: Redirect the user back to the team page and display the message that he can't invite a coach or club

            }
            $invitation->setUser($player);

            //Creating the notification for the user to accept the invitation or not.

            $message = 'Tu es invite pour rejoindre la team :' . $playerTeam->getName(). ' Pour repondre clique sur cette notification.';
            $url =  $this->generateUrl('bf_team_invite_view', array('hash' => $invitation->getHash()));
            $notificationService = $this->get('bf.notification')->create($player, $coach, $message, $url);


            //Send a mail to the Inivted player.



            $this->addFlash( 'notice', 'Le joueur ' . $player->getUsername(). ' a été invité à rejoindre votre team. Dès qu\'il acceptera l\'invitation, il apparaitrera sur cette page !' );


        }
        else{

            //Send a mail to the Inivited player.



            $this->addFlash( 'notice', 'Le joueur ' . $playerEmail. ' a été invité à rejoindre votre team. Dès qu\'il acceptera l\'invitation, il apparaitrera sur cette page !' );

        }

        $em->persist($invitation);
        $em->flush();


        //Redirect the coach back to his my team page. And display a flash message to say that the player has been invited.
        return $this->redirectToRoute('bf_core_team');



    }

    /**
     *
     * @ParamConverter("team_invitation", options={"mapping": {"hash": "hash"}})
     * The action to accept an invitation to a team
     */
    public function invitationResponseAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $hash = $request->request->get('invitation_hash');
        $teamInvitation = $em->getRepository('BFAppBundle:TeamInvitation')->findOneByHash($hash);

        if(empty($teamInvitation) || $teamInvitation == null){
            throw $this->createNotFoundException();
        }

        if($teamInvitation->getUser()->getId() != $this->getUser()->getId()){
            throw $this->createAccessDeniedException();
        }

        if($teamInvitation->getActive() === false){
            $this->addFlash(
                'error',
                'Vous avez deja repondu a cette invitation'
            );

            return $this->redirectToRoute('bf_core_homepage');
        }

        $awnser = $request->request->get('submit');

        //The player replied so put the invitation to non active
        $teamInvitation->setActive(false);

        //The player accepted the awnser
        if($awnser == 1){

            $playerSeasonStats = new PlayerSeasonStats();

            $playerSeasonStats
                ->setTeam($teamInvitation->getTeam())
                ->setUser($teamInvitation->getUser())
                ->setSeason($teamInvitation->getSeason())
                ->setPosition($teamInvitation->getPosition())
            ;

            //Send out a notification to the coach
            $message = $teamInvitation->getUser()->getUsername(). ' a accepte invitation pour rejoindre votre team.';
            $url =  $this->generateUrl('bf_core_team');
            $this->get('bf.notification')->create($teamInvitation->getTeam()->getCoach(), $teamInvitation->getUser(), $message, $url);


            //redirect the user to the my team page and add a  flash message to welcome him.

            $em->persist($playerSeasonStats);
            $em->flush();

            $this->addFlash(
                'notice',
                'Bienvenue dans la team ' . $teamInvitation->getTeam()->getName()
            );

            return $this->redirectToRoute('bf_core_team');

        }
        //The player declined the awnser
        else{

            //Send out a notification to the coach
            $message = $teamInvitation->getUser()->getUsername(). ' a decline votre invitation pour rejoindre votre team.';
            $url =  $this->generateUrl('bf_core_team');
            $this->get('bf.notification')->create($teamInvitation->getTeam()->getCoach(), $teamInvitation->getUser(), $message, $url);

            //Redirect to the homepage.

            $em->flush();

            return $this->redirectToRoute('bf_core_homepage');

        }
    }
}