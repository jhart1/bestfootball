<?php

namespace BF\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BFAppBundle:Default:index.html.twig');
    }
}
