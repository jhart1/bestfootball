<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-12-05
 * Time: 10:16 PM
 */

namespace BF\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use BF\AppBundle\Entity\Fichier;
use BF\AppBundle\Form\FichierType;
use FFMpeg;

class UploadController extends Controller
{
    /**
     *
     * Create the video for the uploaded file.
     */
    public function addFileAction(Request $request)
    {
        $file = new Fichier();
        $file->setFile($request->files->get('file'));
        $allowed = $request->request->get('allowed');
        $allowedFiles = null;
        if($allowed == 'video'){
            $allowedFiles = array("video/avi", "video/quicktime", "video/x-sgi-movie", "video/mpeg", "video/mp4", "video/3gpp", "video/x-flv");
        }
        elseif($allowed == 'picture'){
            $allowedFiles = array("image/jpeg", "image/pjpeg");
        }

        $form = $this->createForm(FichierType::class, $file);
        $form->handleRequest($request);

        if($form){
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($file);

            $mimeType = $file->getFile()->getMimeType();

            //Before we upload the file, check the file type.
            if(in_array($mimeType, $allowedFiles)){
                // On utilise un service pour gérer l'upload du fichier sur le serveur
                $this->get('stof_doctrine_extensions.uploadable.manager')->markEntityToUpload($file, $file->getFile());
                $em->flush();

                // On retourne l'id du fichier dans un tableau JSON
                return new JsonResponse(array(
                    'id' => $file->getId(),
                ));
            }
            else{
                // Si le formulaire n'est pas valide on retourne une erreur en JSON
                return new JsonResponse(array(
                    'error' => (string) "File type is not allowed",
                ), 400);
            }
        }

        // Si le formulaire n'est pas valide on retourne une erreur en JSON
        return new JsonResponse(array(
            'error' => (string) $form->getErrors(true),
        ), 400);

    }
}