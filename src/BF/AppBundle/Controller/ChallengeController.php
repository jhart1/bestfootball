<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2017-01-17
 * Time: 7:43 PM
 */

namespace BF\AppBundle\Controller;

use BF\AppBundle\Entity\Challenge;
use BF\AppBundle\Entity\PlayerSeasonStats;
use BF\AppBundle\Form\ChallengeParticipationType;
use BF\AppBundle\Form\ChallengeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

//entities
use BF\AppBundle\Entity\TeamInvitation;
use BF\AppBundle\Entity\Team;
use BF\UserBundle\Entity\User;

class ChallengeController extends Controller
{
    /**
     *
     * Action to add a challenge to a team
     * This action is only accesible for a coach or club.
     *
     *
     * @ParamConverter("team", options={"mapping": {"slug": "slug"}})
     */
    public function addTeamChallengeAction(Request $request, Team $team){

        //Prepare the challenge object.
        $challenge = new Challenge();
        $challenge
            ->setSponsored(false)
            ->setTeam($team)
        ;

        $form = $this->createForm(ChallengeType::class, $challenge);
        $form
            ->remove('sponsored')
            ->remove('partner')
            ->remove('first_price')
            ->remove('second_price')
            ->remove('third_price')
            ->remove('endDate')
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($challenge);
            $em->flush();

            //Call the convert command to convert the video. Asynchronously
            $command = '/usr/bin/php '.$this->get('kernel')->getRootDir().'/../bin/console app:convert-video:challenge --ID=' . $challenge->getId()." > /dev/null 2>/dev/null &";
            shell_exec($command);

            //Send a flash message to the coach.
            $this->addFlash( 'notice', 'Votre challenge a bien ete ajoute !' );

            //@TODO: Send a notification to all the team members or club members if the challenge is club public

            return $this->redirectToRoute('bf_core_team_challenges', array('teamslug' => $team->getSlug()));
        }

        return $this->render('BFCoreBundle:Team:challenge-add.html.twig', array(
            'form' => $form->createView(),
            'team' =>$team
        ));
    }

    /**
     *
     * Action to add a challenge to a team
     */
    public function modTeamChallengeAction(Request $request){

    }

    /**
     *
     * Action to add a challenge to a team
     */
    public function delTeamChallengeAction(Request $request){

    }

}