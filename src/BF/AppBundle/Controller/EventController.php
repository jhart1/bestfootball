<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2017-01-17
 * Time: 7:44 PM
 */

namespace BF\AppBundle\Controller;

use BF\AppBundle\Entity\PlayerSeasonStats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

//entities
use BF\AppBundle\Entity\TeamInvitation;
use BF\AppBundle\Entity\Team;
use BF\UserBundle\Entity\User;

class EventController extends Controller
{
    /**
     * @param Request $request
     */
    public function addEventAction(Request $request){

    }


    /**
     *
     * Action to add an event to a match, goal, assist, etc....
     *
     * @param Request $request
     */
    public function addMatchEventAction(Request $request){

    }

}