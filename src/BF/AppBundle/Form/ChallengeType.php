<?php

namespace BF\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//other entities
use BF\AppBundle\Form\PriceType;
use BF\AppBundle\Form\PartnerType;



class ChallengeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('one', IntegerType::class)
            ->add('two', IntegerType::class)
            ->add('three', IntegerType::class)
            ->add('four', IntegerType::class)
            ->add('five', IntegerType::class)
            ->add('six', IntegerType::class)
            ->add('endDate', DateType::class)
            ->add('active', CheckboxType::class, array('required' => false))
            ->add('clubPublic', CheckboxType::class, array('required' => false))
            ->add('sponsored', CheckboxType::class, array('required' => false))
            ->add('video', EntityType::class, array(
                'class' => 'BF\AppBundle\Entity\Fichier'
            ))

            //this section is optional and only for partenered challenges
            ->add('partner', PartnerType::class, array('required' => false))
            ->add('first_price', PriceType::class, array('required' => false))
            ->add('second_price', PriceType::class, array('required' => false))
            ->add('third_price', PriceType::class, array('required' => false))

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BF\AppBundle\Entity\Challenge'
        ));
    }
}
