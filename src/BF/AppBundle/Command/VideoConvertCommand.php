<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2017-01-15
 * Time: 8:50 PM
 */

namespace BF\AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use FFMpeg;

class VideoConvertCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:convert-video:challenge-participation')

            // the short description shown while running "php bin/console list"
            ->setDescription('Converts the received video and updates the challenge participation to which it belongs')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Converts the received video and updates the challenge participation to which it belongs")

            ->addOption(
                'ID',
                null,
                InputOption::VALUE_REQUIRED,
                'The path of the challenge participation video',
                1
            );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $challengeParticipationID = $input->getOption('ID');
        $challengeParticipation = $em->getRepository('BFAppBundle:ChallengeParticipation')->find($challengeParticipationID);

        $file = $challengeParticipation->getVideo()->getPath();
        $watermark = $this->getContainer()->get('kernel')->getRootDir().'/../web/assets/images/watermark.png';

        $uploadsFolder = $this->getContainer()->get('kernel')->getRootDir().'/../web/uploads/';

        $filename = sha1(uniqid());
        $thumbnailFileName = $filename .".jpg";
        $videoFileName = $filename . '.mp4';

        //Converting the video file
        $ffmpeg = '/home/joris/bin/ffmpeg';
        $cmd1 = $ffmpeg.' -i '.$file.' -codec:v libx264 -profile:v high -preset slow -b:v 1M -maxrate 1M -bufsize 2M -threads 0 -filter:v "scale=iw*min(640/iw\,360/ih):ih*min(640/iw\,360/ih),pad=640:360:(640-iw)/2:(360-ih)/2" -codec:a libmp3lame -b:a 128k '. $uploadsFolder . $filename.'_converted_scaled.mp4 1> '.$uploadsFolder.'block_'.$filename.'.txt 2>&1';
        shell_exec($cmd1);

        //Putting the watermark onto the video
        $cmd2 = $ffmpeg.' -i '.$uploadsFolder.$filename.'_converted_scaled.mp4 -i ' . $watermark . ' -filter_complex "overlay=10:10" '.$uploadsFolder.'videos/'.$filename.'.mp4 1> '.$uploadsFolder.'block2_'.$filename.'.txt 2>&1';
        shell_exec($cmd2);
        unlink($uploadsFolder.$filename.'_converted_scaled.mp4');

        //thumbnail making
        $cmd3 = $ffmpeg.' -i '.$uploadsFolder.'videos/'.$filename.'.mp4 -ss 00:00:02 -vframes 1 '.$uploadsFolder.'videos/thumbnails/'.$filename.'.jpg';
        shell_exec($cmd3);

        //Unlinking the progress files.
        unlink($uploadsFolder.'block_'.$filename.'.txt');
        unlink($uploadsFolder.'block2_'.$filename.'.txt');

        //Output the file paths for the video and the thumbnail.
        $challengeParticipation
            ->setThumbnail("/uploads/videos/thumbnails/" . $thumbnailFileName)
            ->setVideoSRC("/uploads/videos/" . $videoFileName);

        $em->persist($challengeParticipation);
        $em->flush();

        $output->writeln('Finished converting the video!');
    }
}