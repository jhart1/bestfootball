<?php

namespace BF\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="BF\AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="opponent", type="string", length=255)
     */
    private $opponent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255)
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="selfScore", type="integer")
     */
    private $selfScore;

    /**
     * @var string
     *
     * @ORM\Column(name="opponentScore", type="integer")
     */
    private $opponentScore;


    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set opponent
     *
     * @param string $opponent
     *
     * @return Game
     */
    public function setOpponent($opponent)
    {
        $this->opponent = $opponent;

        return $this;
    }

    /**
     * Get opponent
     *
     * @return string
     */
    public function getOpponent()
    {
        return $this->opponent;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Game
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Game
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set selfScore
     *
     * @param integer $selfScore
     *
     * @return Game
     */
    public function setSelfScore($selfScore)
    {
        $this->selfScore = $selfScore;

        return $this;
    }

    /**
     * Get selfScore
     *
     * @return int
     */
    public function getSelfScore()
    {
        return $this->selfScore;
    }

    /**
     * Set opponentScore
     *
     * @param string $opponentScore
     *
     * @return Game
     */
    public function setOpponentScore($opponentScore)
    {
        $this->opponentScore = $opponentScore;

        return $this;
    }

    /**
     * Get opponentScore
     *
     * @return string
     */
    public function getOpponentScore()
    {
        return $this->opponentScore;
    }
}

