<?php

namespace BF\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChallengeParticipation
 *
 * @ORM\Table(name="challenge_participation")
 * @ORM\Entity(repositoryClass="BF\AppBundle\Repository\ChallengeParticipationRepository")
 */
class ChallengeParticipation
{

    /**
     * The Challenge to which the participation is related
     *
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Challenge")
     * @ORM\JoinColumn(nullable=false)
     */
    private $challenge;

    /**
     * The User to which the participation is related
     *
     * @ORM\ManyToOne(targetEntity="BF\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * The video that is related to the participation
     *
     * @ORM\OneToOne(targetEntity="BF\AppBundle\Entity\Fichier")
     */
    private $video;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="repetitions", type="integer")
     */
    private $repetitions;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var int
     *
     * @ORM\Column(name="public", type="boolean")
     */
    private $public;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="video_src", type="string", length=255, nullable=true)
     */
    private $videoSRC;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \Datetime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set challenge
     *
     * @param \BF\AppBundle\Entity\Challenge $challenge
     *
     * @return ChallengeParticipation
     */
    public function setChallenge(\BF\AppBundle\Entity\Challenge $challenge)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * Get challenge
     *
     * @return \BF\AppBundle\Entity\Challenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }

    /**
     * Set user
     *
     * @param \BF\UserBundle\Entity\User $user
     *
     * @return ChallengeParticipation
     */
    public function setUser(\BF\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BF\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set repetitions
     *
     * @param integer $repetitions
     *
     * @return ChallengeParticipation
     */
    public function setRepetitions($repetitions)
    {
        $this->repetitions = $repetitions;

        return $this;
    }

    /**
     * Get repetitions
     *
     * @return integer
     */
    public function getRepetitions()
    {
        return $this->repetitions;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return ChallengeParticipation
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return ChallengeParticipation
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ChallengeParticipation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set video
     *
     * @param \BF\AppBundle\Entity\Fichier $video
     *
     * @return ChallengeParticipation
     */
    public function setVideo(\BF\AppBundle\Entity\Fichier $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \BF\AppBundle\Entity\Fichier
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     *
     * @return ChallengeParticipation
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set videoSRC
     *
     * @param string $videoSRC
     *
     * @return ChallengeParticipation
     */
    public function setVideoSRC($videoSRC)
    {
        $this->videoSRC = $videoSRC;

        return $this;
    }

    /**
     * Get videoSRC
     *
     * @return string
     */
    public function getVideoSRC()
    {
        return $this->videoSRC;
    }
}
