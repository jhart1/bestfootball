<?php

namespace BF\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlayerSeasonStats
 *
 * @ORM\Table(name="bf_player_season_stats")
 * @ORM\Entity(repositoryClass="BF\AppBundle\Repository\PlayerSeasonStatsRepository")
 */
class PlayerSeasonStats
{
    //relations between the different entities
    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Team", inversedBy="playersSeasonStats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="BF\UserBundle\Entity\User", inversedBy="playerSeasonStatistics")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Season")
     * @ORM\JoinColumn(nullable=false)
     */
    private $season;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="Captain", type="boolean")
     */
    private $captain;

    /**
     * @var int
     *
     * @ORM\Column(name="Number", type="integer")
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="NumberMatches", type="integer")
     */
    private $numberMatches;

    /**
     * @var int
     *
     * @ORM\Column(name="NumberTrainings", type="integer")
     */
    private $numberTrainings;

    /**
     * @var int
     *
     * @ORM\Column(name="NumberGoals", type="integer")
     */
    private $numberGoals;

    /**
     * @var int
     *
     * @ORM\Column(name="NumberAssists", type="integer")
     */
    private $numberAssists;

    /**
     * @var int
     *
     * @ORM\Column(name="NumberYellow", type="integer")
     */
    private $numberYellow;

    /**
     * @var int
     *
     * @ORM\Column(name="NumberRed", type="integer")
     */
    private $numberRed;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;

    public function __construct()
    {
        //setting the fields to 0
        $this->captain = 0;
        $this->number = 0;
        $this->numberMatches = 0;
        $this->numberTrainings = 0;
        $this->numberGoals = 0;
        $this->numberAssists = 0;
        $this->numberYellow = 0;
        $this->numberRed = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set captain
     *
     * @param boolean $captain
     *
     * @return PlayerSeasonStats
     */
    public function setCaptain($captain)
    {
        $this->captain = $captain;

        return $this;
    }

    /**
     * Get captain
     *
     * @return bool
     */
    public function getCaptain()
    {
        return $this->captain;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return PlayerSeasonStats
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set numberMatches
     *
     * @param integer $numberMatches
     *
     * @return PlayerSeasonStats
     */
    public function setNumberMatches($numberMatches)
    {
        $this->numberMatches = $numberMatches;

        return $this;
    }

    /**
     * Get numberMatches
     *
     * @return int
     */
    public function getNumberMatches()
    {
        return $this->numberMatches;
    }

    /**
     * Set numberTrainings
     *
     * @param integer $numberTrainings
     *
     * @return PlayerSeasonStats
     */
    public function setNumberTrainings($numberTrainings)
    {
        $this->numberTrainings = $numberTrainings;

        return $this;
    }

    /**
     * Get numberTrainings
     *
     * @return integer
     */
    public function getNumberTrainings()
    {
        return $this->numberTrainings;
    }

    /**
     * Set numberGoals
     *
     * @param integer $numberGoals
     *
     * @return PlayerSeasonStats
     */
    public function setNumberGoals($numberGoals)
    {
        $this->numberGoals = $numberGoals;

        return $this;
    }

    /**
     * Get numberGoals
     *
     * @return int
     */
    public function getNumberGoals()
    {
        return $this->numberGoals;
    }

    /**
     * Set numberAssists
     *
     * @param integer $numberAssists
     *
     * @return PlayerSeasonStats
     */
    public function setNumberAssists($numberAssists)
    {
        $this->numberAssists = $numberAssists;

        return $this;
    }

    /**
     * Get numberAssists
     *
     * @return int
     */
    public function getNumberAssists()
    {
        return $this->numberAssists;
    }

    /**
     * Set numberYellow
     *
     * @param integer $numberYellow
     *
     * @return PlayerSeasonStats
     */
    public function setNumberYellow($numberYellow)
    {
        $this->numberYellow = $numberYellow;

        return $this;
    }

    /**
     * Get numberYellow
     *
     * @return int
     */
    public function getNumberYellow()
    {
        return $this->numberYellow;
    }

    /**
     * Set numberRed
     *
     * @param integer $numberRed
     *
     * @return PlayerSeasonStats
     */
    public function setNumberRed($numberRed)
    {
        $this->numberRed = $numberRed;

        return $this;
    }

    /**
     * Get numberRed
     *
     * @return int
     */
    public function getNumberRed()
    {
        return $this->numberRed;
    }

    /**
     * Set team
     *
     * @param \BF\AppBundle\Entity\Team $team
     *
     * @return PlayerSeasonStats
     */
    public function setTeam(\BF\AppBundle\Entity\Team $team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \BF\AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set user
     *
     * @param \BF\UserBundle\Entity\User $user
     *
     * @return PlayerSeasonStats
     */
    public function setUser(\BF\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BF\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set season
     *
     * @param \BF\AppBundle\Entity\Season $season
     *
     * @return PlayerSeasonStats
     */
    public function setSeason(\BF\AppBundle\Entity\Season $season)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return \BF\AppBundle\Entity\Season
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return PlayerSeasonStats
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }
}
