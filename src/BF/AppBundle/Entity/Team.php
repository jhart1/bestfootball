<?php

namespace BF\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="BF\AppBundle\Repository\TeamRepository")
 */
class Team
{

    //relations between the different entities
    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Club", inversedBy="teams")
     * @ORM\JoinColumn(nullable=false)
     */
    private $club;

    /**
     * @ORM\OneToOne(targetEntity="BF\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $coach;


    //Bidirectional relations
    /**
     * @ORM\OneToMany(targetEntity="BF\AppBundle\Entity\PlayerSeasonStats", mappedBy="team")
     */
    private $playersSeasonStats;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->statsplayers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->active = true;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set club
     *
     * @param \BF\AppBundle\Entity\Club $club
     *
     * @return Team
     */
    public function setClub(\BF\AppBundle\Entity\Club $club)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get club
     *
     * @return \BF\AppBundle\Entity\Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Set coach
     *
     * @param \BF\UserBundle\Entity\User $coach
     *
     * @return Team
     */
    public function setCoach(\BF\UserBundle\Entity\User $coach = null)
    {
        $this->coach = $coach;

        return $this;
    }

    /**
     * Get coach
     *
     * @return \BF\UserBundle\Entity\User
     */
    public function getCoach()
    {
        return $this->coach;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Team
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add playersSeasonStat
     *
     * @param \BF\AppBundle\Entity\PlayerSeasonStats $playersSeasonStat
     *
     * @return Team
     */
    public function addPlayersSeasonStat(\BF\AppBundle\Entity\PlayerSeasonStats $playersSeasonStat)
    {
        $this->playersSeasonStats[] = $playersSeasonStat;

        return $this;
    }

    /**
     * Remove playersSeasonStat
     *
     * @param \BF\AppBundle\Entity\PlayerSeasonStats $playersSeasonStat
     */
    public function removePlayersSeasonStat(\BF\AppBundle\Entity\PlayerSeasonStats $playersSeasonStat)
    {
        $this->playersSeasonStats->removeElement($playersSeasonStat);
    }

    /**
     * Get playersSeasonStats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayersSeasonStats()
    {
        return $this->playersSeasonStats;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Team
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
