<?php

namespace BF\AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Challenge
 *
 * @ORM\Table(name="bf_challenge")
 * @ORM\Entity(repositoryClass="BF\AppBundle\Repository\ChallengeRepository")
 */
class Challenge
{

    //the partner entity for if the challenge is sponsored
    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Partner", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $partner;

    //the price entity for if the challenge has some prices
    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Price", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $first_price;

    //the price entity for if the challenge has some prices
    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Price", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $second_price;

    //the price entity for if the challenge has some prices
    /**
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Price", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $third_price;

    /**
     * The video that is related to the participation
     *
     * @ORM\OneToOne(targetEntity="BF\AppBundle\Entity\Fichier")
     */
    private $video;

    /**
     * The Team to which the challenge is intended
     *
     * @ORM\ManyToOne(targetEntity="BF\AppBundle\Entity\Team")
     * @ORM\JoinColumn(nullable=true)
     */
    private $team;



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail_src", type="string", length=255, nullable=true)
     */
    private $thumbnailSource;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail_alt", type="string", length=255, nullable=true)
     */
    private $thumbnailAlt;

    /**
     * @var string
     *
     * @ORM\Column(name="one", type="integer", length=10)
     */
    private $one;

    /**
     * @var string
     *
     * @ORM\Column(name="two", type="integer", length=10)
     */
    private $two;
    /**
     * @var string
     *
     * @ORM\Column(name="three", type="integer", length=10)
     */
    private $three;

    /**
     * @var string
     *
     * @ORM\Column(name="four", type="integer", length=10)
     */
    private $four;

    /**
     * @var string
     *
     * @ORM\Column(name="five", type="integer", length=10)
     */
    private $five;

    /**
     * @var string
     *
     * @ORM\Column(name="six", type="integer", length=10)
     */
    private $six;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="endDate", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(name="sponsored", type="boolean")
     */
    private $sponsored;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(name="club_public", type="boolean")
     */
    private $club_public;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="video_src", type="string", length=255, nullable=true)
     */
    private $videoSRC;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Challenge
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Challenge
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set videoSource
     *
     * @param string $videoSource
     *
     * @return Challenge
     */
    public function setVideoSource($videoSource)
    {
        $this->videoSource = $videoSource;

        return $this;
    }

    /**
     * Get videoSource
     *
     * @return string
     */
    public function getVideoSource()
    {
        return $this->videoSource;
    }

    /**
     * Set imageSource
     *
     * @param string $imageSource
     *
     * @return Challenge
     */
    public function setImageSource($imageSource)
    {
        $this->imageSource = $imageSource;

        return $this;
    }

    /**
     * Get imageSource
     *
     * @return string
     */
    public function getImageSource()
    {
        return $this->imageSource;
    }

    /**
     * Set imageAlt
     *
     * @param string $imageAlt
     *
     * @return Challenge
     */
    public function setImageAlt($imageAlt)
    {
        $this->imageAlt = $imageAlt;

        return $this;
    }

    /**
     * Get imageAlt
     *
     * @return string
     */
    public function getImageAlt()
    {
        return $this->imageAlt;
    }

    /**
     * Set one
     *
     * @param integer $one
     *
     * @return Challenge
     */
    public function setOne($one)
    {
        $this->one = $one;

        return $this;
    }

    /**
     * Get one
     *
     * @return integer
     */
    public function getOne()
    {
        return $this->one;
    }

    /**
     * Set two
     *
     * @param integer $two
     *
     * @return Challenge
     */
    public function setTwo($two)
    {
        $this->two = $two;

        return $this;
    }

    /**
     * Get two
     *
     * @return integer
     */
    public function getTwo()
    {
        return $this->two;
    }

    /**
     * Set three
     *
     * @param integer $three
     *
     * @return Challenge
     */
    public function setThree($three)
    {
        $this->three = $three;

        return $this;
    }

    /**
     * Get three
     *
     * @return integer
     */
    public function getThree()
    {
        return $this->three;
    }

    /**
     * Set four
     *
     * @param integer $four
     *
     * @return Challenge
     */
    public function setFour($four)
    {
        $this->four = $four;

        return $this;
    }

    /**
     * Get four
     *
     * @return integer
     */
    public function getFour()
    {
        return $this->four;
    }

    /**
     * Set five
     *
     * @param integer $five
     *
     * @return Challenge
     */
    public function setFive($five)
    {
        $this->five = $five;

        return $this;
    }

    /**
     * Get five
     *
     * @return integer
     */
    public function getFive()
    {
        return $this->five;
    }

    /**
     * Set six
     *
     * @param integer $six
     *
     * @return Challenge
     */
    public function setSix($six)
    {
        $this->six = $six;

        return $this;
    }

    /**
     * Get six
     *
     * @return integer
     */
    public function getSix()
    {
        return $this->six;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Challenge
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Challenge
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Challenge
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set partner
     *
     * @param \BF\AppBundle\Entity\Partner $partner
     *
     * @return Challenge
     */
    public function setPartner(\BF\AppBundle\Entity\Partner $partner = null)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner
     *
     * @return \BF\AppBundle\Entity\Partner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set firstPrice
     *
     * @param \BF\AppBundle\Entity\Price $firstPrice
     *
     * @return Challenge
     */
    public function setFirstPrice(\BF\AppBundle\Entity\Price $firstPrice = null)
    {
        $this->first_price = $firstPrice;

        return $this;
    }

    /**
     * Get firstPrice
     *
     * @return \BF\AppBundle\Entity\Price
     */
    public function getFirstPrice()
    {
        return $this->first_price;
    }

    /**
     * Set secondPrice
     *
     * @param \BF\AppBundle\Entity\Price $secondPrice
     *
     * @return Challenge
     */
    public function setSecondPrice(\BF\AppBundle\Entity\Price $secondPrice = null)
    {
        $this->second_price = $secondPrice;

        return $this;
    }

    /**
     * Get secondPrice
     *
     * @return \BF\AppBundle\Entity\Price
     */
    public function getSecondPrice()
    {
        return $this->second_price;
    }

    /**
     * Set thirdPrice
     *
     * @param \BF\AppBundle\Entity\Price $thirdPrice
     *
     * @return Challenge
     */
    public function setThirdPrice(\BF\AppBundle\Entity\Price $thirdPrice = null)
    {
        $this->third_price = $thirdPrice;

        return $this;
    }

    /**
     * Get thirdPrice
     *
     * @return \BF\AppBundle\Entity\Price
     */
    public function getThirdPrice()
    {
        return $this->third_price;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Challenge
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set sponsored
     *
     * @param boolean $sponsored
     *
     * @return Challenge
     */
    public function setSponsored($sponsored)
    {
        $this->sponsored = $sponsored;

        return $this;
    }

    /**
     * Get sponsored
     *
     * @return boolean
     */
    public function getSponsored()
    {
        return $this->sponsored;
    }

    /**
     * Set video
     *
     * @param \BF\AppBundle\Entity\Fichier $video
     *
     * @return Challenge
     */
    public function setVideo(\BF\AppBundle\Entity\Fichier $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \BF\AppBundle\Entity\Fichier
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set clubPublic
     *
     * @param boolean $clubPublic
     *
     * @return Challenge
     */
    public function setClubPublic($clubPublic)
    {
        $this->club_public = $clubPublic;

        return $this;
    }

    /**
     * Get clubPublic
     *
     * @return boolean
     */
    public function getClubPublic()
    {
        return $this->club_public;
    }

    /**
     * Set team
     *
     * @param \BF\AppBundle\Entity\Team $team
     *
     * @return Challenge
     */
    public function setTeam(\BF\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \BF\AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set thumbnailSource
     *
     * @param string $thumbnailSource
     *
     * @return Challenge
     */
    public function setThumbnailSource($thumbnailSource)
    {
        $this->thumbnailSource = $thumbnailSource;

        return $this;
    }

    /**
     * Get thumbnailSource
     *
     * @return string
     */
    public function getThumbnailSource()
    {
        return $this->thumbnailSource;
    }

    /**
     * Set thumbnailAlt
     *
     * @param string $thumbnailAlt
     *
     * @return Challenge
     */
    public function setThumbnailAlt($thumbnailAlt)
    {
        $this->thumbnailAlt = $thumbnailAlt;

        return $this;
    }

    /**
     * Get thumbnailAlt
     *
     * @return string
     */
    public function getThumbnailAlt()
    {
        return $this->thumbnailAlt;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     *
     * @return Challenge
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set videoSRC
     *
     * @param string $videoSRC
     *
     * @return Challenge
     */
    public function setVideoSRC($videoSRC)
    {
        $this->videoSRC = $videoSRC;

        return $this;
    }

    /**
     * Get videoSRC
     *
     * @return string
     */
    public function getVideoSRC()
    {
        return $this->videoSRC;
    }
}
