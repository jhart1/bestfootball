<?php
/**
 * Created by PhpStorm.
 * User: Joris
 * Date: 2016-12-07
 * Time: 10:10 PM
 */

namespace BF\AppBundle\Participation;

use Doctrine\ORM\EntityManager;
use BF\AppBundle\Entity\ChallengeParticipation;

class BFParticipationPoints
{
    protected $doctrine;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param ChallengeParticipation $participation
     * @return int $score
     */
    public function getParticipationPoints(ChallengeParticipation $participation)
    {
        $repetions = $participation->getRepetitions();
        $challenge = $participation->getChallenge();
        $score = 0;

        //Look up in which trench the participation is.

        if( $repetions < $challenge->getSix()){
            $score = 0;
        }
        elseif (  $challenge->getSix() <= $repetions && $repetions < $challenge->getFive() ){
            $score = 50;
        }
        elseif (  $challenge->getFive() <= $repetions && $repetions < $challenge->getFour() ){
            $score = 100;
        }
        elseif (  $challenge->getFour() <= $repetions && $repetions < $challenge->getThree() ){
            $score = 150;
        }
        elseif (  $challenge->getThree() <= $repetions && $repetions < $challenge->getTwo() ){
            $score = 200;
        }
        elseif (  $challenge->getTwo() <= $repetions && $repetions < $challenge->getOne() ){
            $score = 250;
        }
        elseif (  $challenge->getOne() <= $repetions ){
            $score = 300;
        }

        return (int) $score;
    }


    /**
     *
     * Update the score of the user and recount his points.
     *
     * @param ChallengeParticipation $participation
     *
     * @return int $points
     */
    public function getUserPoints(ChallengeParticipation $participation, $user){
        $challenge = $participation->getChallenge();
        $newUserScore = 0;

        //Get the participations for the same Challenge.
        $sameParticipations = $this->em->getRepository('BFAppBundle:ChallengeParticipation')->getSameUserParticipations($user, $challenge);

        if(!empty($sameParticipations)){
            $best_participation_score = $sameParticipations[0]->getScore();

            if($participation->getScore() > $best_participation_score){
                //give the user 20 points extra.
                $newUserScore = $participation->getScore() + 20;
            }
            else{
                $newUserScore = $best_participation_score;
            }
        }
        else{
            $newUserScore = $participation->getScore();
        }

        //Calculate the point for the other challenges the user has.
        $otherChallenges = $this->em->getRepository('BFAppBundle:Challenge')->findAll();
        foreach ($otherChallenges as $otherChallenge){
            if($otherChallenge->getId() != $challenge->getId()){

                $otherParticipations = $this->em->getRepository('BFAppBundle:ChallengeParticipation')->getSameUserParticipations($user, $otherChallenge);

                if(!empty($otherParticipations) && $otherParticipations != null){

                    //Only one participation, so use the normal score
                    if(count($otherParticipations) == 1){
                        $newUserScore = $newUserScore + $otherParticipations[0]->getScore();
                    }
                    else{
                        //The highest scoring challenge is the
                        if($otherParticipations[0]->getScore() > $otherParticipations[1]->getScore() && $otherParticipations[0]->getDate() > $otherParticipations[1]->getDate()){
                            $newUserScore = $newUserScore + $otherParticipations[0]->getScore()+20;
                        }
                        else{
                            $newUserScore = $newUserScore + $otherParticipations[0]->getScore();
                        }
                    }
                }
            }
        }

        return $newUserScore;
    }



}