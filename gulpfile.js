var gulp = require('gulp'),   
    sass = require('gulp-ruby-sass'),
    notify = require("gulp-notify"),
    bower = require('gulp-bower'),
    minify = require('gulp-minify'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify');

var config = {
    sassPath: './resources/sass',
    scriptPath: './resources/js',
    bowerDir: './web/assets/vendor'
}

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});

gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/font-awesome/fonts/**.*') 
        .pipe(gulp.dest('./web/assets/fonts')); 
});

gulp.task('css', function() {
    return gulp.src(config.sassPath + '/main.scss')
        .pipe(sass({
            style: 'compressed',
            loadPath: [
                './resources/sass',
                config.bowerDir + '/bootstrap-sass/assets/stylesheets',
                config.bowerDir + '/font-awesome/scss',
            ]
        })
            .on("error", notify.onError(function (error) {
                return "Error: " + error.message;
            })))
        .pipe(gulp.dest('./web/assets/css'));
});

/* Scripts
 ====================================== */

gulp.task('scripts', function () {
    gulp.src(config.scriptPath + '/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./web/assets/js'));
});


// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(config.sassPath + '/**/*.scss', ['css']);
    gulp.watch(config.scriptPath + '/**/*.js', ['scripts']);
});


gulp.task('images', function() {
    gulp.src('resources/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('web/assets/images'))
});


gulp.task('default', ['bower', 'icons', 'images', 'css', 'scripts']);


