var limit = 5;
var offset = 0;
var unreadConversationsLabel = $('#user-messages').find('span.badge');
var conversationsBox = $('#user-messages').find('.dropdown-menu');


function $_GET(param) {
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if ( param ) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}

function getConversations(limit, offset){
    var url = Routing.generate('bf_conversations_get');
    data = 'offset=' + offset + '&limit=' + limit;

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(response){
            var unreadConversation = 0,
                outputConversation = '';

            $.each( response, function ( i, conversation ) {
                if(conversation.read == 1){
                    outputConversation +=  '<li>';
                }
                else{
                    outputConversation +=  '<li class="unread">';
                    unreadConversation++
                }
                outputConversation +=  '<div class="conversation-wrapper">';
                outputConversation += '<img src="' + conversation.otherUserAvatar + '" />';
                outputConversation += '<div class="conversation-text-wrapper">';
                outputConversation += '<p class="conversation-title">' + conversation.otherUsername + '</p>';
                outputConversation += '<p>' + conversation.lastMessage + '</p>';
                outputConversation += '<span class="conversation-date">' + conversation.lastMessageDate + '</span></div>';
                outputConversation +=  '</div>';
                outputConversation +=  '</li>';
            });

            //Append the number of unread conversations.
            if(unreadConversation > 0){
                unreadConversationsLabel.html(unreadConversation).show();
            }
            conversationsBox.append(outputConversation);
        },
        error: function(response){
            console.log('error');
        }
    });
}

function getConversation(){

}

function markConversationAsRead(id){
    var url = Routing.generate('bf_notification_mark_read');

    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(response){
            console.log('notif marked read');
            getNotViewedNumber();
        },
        error: function(response){
            console.log('error');
        }
    });

}

$(function() {
    getConversations(limit, offset);
});
