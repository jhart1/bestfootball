var number = 5;
var offset = 0;
var  notificationsBox = $('#user-notifications').find('.dropdown-menu');


/*The function to append the notifications */
function getNotifications(number, offset, notificationsBox){
    var url = Routing.generate('bf_notification_get'),
        data = 'offset=' + offset + '&number=' + number;

    if (notificationsBox.find('li').length == 0){
        var output = '';
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function(response){
                $.each( response, function ( i, notif ) {
                    if(notif.viewed == 1){
                        output +=  '<li>';
                    }
                    else{
                        output +=  '<li class="new">';
                    }
                    output +=  '<div class="notif-wrapper"><a href="' + notif.link + '?ref=notif&notif-id=' + notif.id + '" class="notif-link">';
                    output += '<img src="' + notif.sender_avatar + '" />';
                    output += '<div class="notif-text-wrapper"><p>' + notif.message + '</p>';
                    output += '<span class="notif-date">' + notif.time_passed + ' ago</span></div>';
                    output +=  '</a></div>';
                    output +=  '</li>';
                });
                notificationsBox.append(output);
            },
            error: function(response){
                notificationsBox.append('<li>Error</li>');
            }
        });
    }
}

function getNotViewedNumber(){
    var url = Routing.generate('bf_notification_get_not_viewed');
    var NotificationsLabel = $('#user-notifications').find('span.badge');

    $.ajax({
        type: 'POST',
        url: url,
        success: function(response){
            //console.log(response);
            if(response > 0){
                NotificationsLabel.html(response).show();
                //console.log('showing');
            }
        },
        error: function(response){
            console.log('error');
        }
    });
}

function markNotificationAsRead(id){
    var url = Routing.generate('bf_notification_mark_read');

    $.ajax({
        type: 'POST',
        url: url,
        data: 'id=' + id,
        success: function(response){
            console.log('notif marked read');
            getNotViewedNumber();
        },
        error: function(response){
            console.log('error');
        }
    });

}

function $_GET(param) {
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if ( param ) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}



//Main function off the is JS.

$(function() {
    if( $_GET('ref') && $_GET('notif-id')){
        var id = $_GET('notif-id');
        markNotificationAsRead(id);
    }
    else{
        getNotViewedNumber();
    }

    getNotifications(number, offset, notificationsBox);
});
